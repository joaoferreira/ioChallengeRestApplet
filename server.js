var express = require('express'),
app = express(),
port = process.env.PORT || 3000;
app.listen(port);
app.get('/aluno', 
	function(req, res) { 
		var id = req.query.id; 

		var MongoClient = require('mongodb').MongoClient;
		var ObjectId = require('mongodb').ObjectID;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		var data;

		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db("iochallenge");
			dbo.collection("alunos").find(ObjectId(id)).toArray(function(err, result) {
				if (err) throw err;
				res.json({data : result});
				db.close();
			});
		});
	})

app.get('/ListarAlunos', 
	function(req, res) { 
		var MongoClient = require('mongodb').MongoClient;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		var data;

		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db("iochallenge");
			dbo.collection("alunos").find({}).toArray(function(err, result) {
				if (err) throw err;
				res.json({data : result});
				db.close();
			});
		});
	})

app.get('/sessao', 
	function(req, res) { 
		var id = req.query.id; 

		var MongoClient = require('mongodb').MongoClient;
		var ObjectId = require('mongodb').ObjectID;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		var data;

		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db("iochallenge");
			dbo.collection("sessoes").find(ObjectId(id)).toArray(function(err, result) {
				if (err) throw err;
				res.json({data : result});
				db.close();
			});
		});
	})

app.get('/ListarSessoes', 
	function(req, res) { 
		var MongoClient = require('mongodb').MongoClient;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		var data;

		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db("iochallenge");
			dbo.collection("sessoes").find({}).toArray(function(err, result) {
				if (err) throw err;
				res.json({data : result});
				db.close();
			});
		});
	})

app.get('/presenca', 
	function(req, res) { 
		var id = req.query.id; 

		var MongoClient = require('mongodb').MongoClient;
		var ObjectId = require('mongodb').ObjectID;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		var data;

		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db("iochallenge");
			var query = { id_sessao: ObjectId(id) };
			dbo.collection("presencas").find(ObjectId(id)).toArray(function(err, result) {
				if (err) throw err;
				res.json({data : result});
				db.close();
			});
		});
	})

app.get('/ListarPresencas', 
	function(req, res) { 
		var MongoClient = require('mongodb').MongoClient;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		var data;

		MongoClient.connect(url, function(err, db) {
			if (err) throw err;
			var dbo = db.db("iochallenge");
			dbo.collection("presencas").find({}).toArray(function(err, result) {
				if (err) throw err;
				res.json({data : result});
				db.close();
			});
		});
	})

app.post('/registarAluno', 
	function(req, res) { 
		var MongoClient = require('mongodb').MongoClient;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		
		var name = req.query.nome;
		var number = req.query.numero;
		var grad = req.query.curso;

		if (name != null && number != null && grad != null) {
			MongoClient.connect(url, function(err, db) {
				if (err) throw err;
				 var dbo = db.db("iochallenge");
				  var myobj = { nome: name, numero_aluno: number, curso: grad };
				  dbo.collection("alunos").insertOne(myobj, function(err, result) {
					   if (err) throw err;
					   res.json({data : "Aluno Registado com sucesso!"});
					db.close();
				  });
			});
		} else {
			res.json({data : "Erro! Dados imcompletos!"});
		}
	})

app.post('/registarSessao', 
	function(req, res) { 
		var MongoClient = require('mongodb').MongoClient;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		
		var discipline = req.query.disciplina;
		var type = req.query.tipo; 
		var start = req.query.data_inicio; 
		var end = req.query.data_fim;
		var grad = req.query.curso;
		var teacher = req.query.docente; 

		if (discipline != null && type != null && start != null && end != null && grad != null && teacher != null) {
			MongoClient.connect(url, function(err, db) {
				if (err) throw err;
				 var dbo = db.db("iochallenge");
				  var myobj = { disciplina: discipline, tipo: type, data_inicio: start, data_fim: end, curso: grad, docente: teacher };
				  dbo.collection("sessoes").insertOne(myobj, function(err, result) {
					   if (err) throw err;
					   res.json({data : "Sessão Registada com sucesso!"});
					db.close();
				  });
			});
		} else {
			res.json({data : "Erro! Dados imcompletos!"});
		}
	})

app.post('/registarPresencas', 
	function(req, res) { 
		var MongoClient = require('mongodb').MongoClient;
		var url = "mongodb://joaoferreira:iochallenge@35.189.106.52:27017/iochallenge";
		
		var discipline = req.query.sessao;
		var start = req.query.data_inicio; 
		var end = req.query.data_fim;
		var attendances = req.query.presencas; 

		if (discipline != null && start != null && end != null && attendances != null) {
			MongoClient.connect(url, function(err, db) {
				if (err) throw err;
				 var dbo = db.db("iochallenge");
				  var myobj = { sessao: discipline, data_inicio: start, data_fim: end, presencas: attendances };
				  dbo.collection("presencas").insertOne(myobj, function(err, result) {
					   if (err) throw err;
					   res.json({data : "Presenças Registadas com sucesso!"});
					db.close();
				  });
			});
		} else {
			res.json({data : "Erro! Dados imcompletos!"});
		}
	})
console.log('Message RESTful API server started on: ' + port);